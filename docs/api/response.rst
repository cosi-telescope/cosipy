response
========

Detector response matrices

SpecResponse
------------

.. autoclass:: cosipy.SpecResponse
   :show-inheritance:
   :members:
   :inherited-members:
